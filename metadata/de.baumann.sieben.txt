Categories:Sports & Health
License:GPLv3+
Web Site:https://github.com/scoute-dich/Sieben/blob/HEAD/README.md
Source Code:https://github.com/scoute-dich/Sieben
Issue Tracker:https://github.com/scoute-dich/Sieben/issues
Changelog:https://github.com/scoute-dich/Sieben/blob/HEAD/CHANGELOG.md

Name:Sieben
Auto Name:Sieben      //Menu
Summary:Helps to organize and perform workouts
Description:
Companion app that helps you to perform the seven minutes workout. It's very
basic:

* you can perform the workout
* pause and resume an exercice
* jump to previous or next exercice
* english and german

[https://github.com/scoute-dich/Sieben/blob/HEAD/SCREENSHOTS.md Screenshots]
.

Repo Type:git
Repo:https://github.com/scoute-dich/Sieben

Build:1.1,2
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2
